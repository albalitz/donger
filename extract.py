#!/usr/bin/env python3
import argparse
import json
import sys

import requests
from requests.exceptions import HTTPError
from bs4 import BeautifulSoup


BASE_URL = "http://dongerlist.com"

DEBUG = False


def debug(msg):
    if DEBUG:
        print(msg, file = sys.stderr)


def categories():
    debug("Building category list...")
    response = requests.get(BASE_URL)
    response.raise_for_status()
    html = response.text
    soup = BeautifulSoup(html, "html.parser")

    category_list = soup.find("select", "select-menu")
    debug(f"Found {len(category_list)} categories.")
    for option in category_list.find_all("option"):
        category_name = option.string
        category_url = option["value"]

        if category_name.lower() == "all":
            continue

        debug(f"Found category: {category_name}")
        yield category_name


def extract_from_page(url):
    debug(f"Extracting dongers from page: {url} ...")
    response = requests.get(url)
    response.raise_for_status()
    html = response.text
    soup = BeautifulSoup(html, "html.parser")

    donger_elements = soup.find_all("textarea", "donger")
    for element in donger_elements:
        yield element.text


def extract_from_category(category, page=1):
    debug(f"Extracting from category: {category} ...")
    dongers = []
    while True:
        page_url = f"{BASE_URL}/category/{category}"
        if page > 1:
            page_url += f"/{page}"

        debug(f"Extracting from page {page_url}")
        try:
            dongers_on_page = extract_from_page(page_url)
            dongers.extend(list(dongers_on_page))
        except HTTPError:
            break

        page += 1

    return dongers


def extract_dongers():
    dongers = {}
    debug("Extracting dongers...")

    for category in categories():
        for donger in extract_from_category(category):
            dongers[category] = dongers.get(category, []) + [donger]
        debug(f"Found {len(dongers[category])} dongers in {category}")

    return dongers


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Extract dongers from dongerlist.com")

    parser.add_argument(
        "--debug",
        action = "store_true",
        help = "Enable debug output"
    )
    parser.add_argument(
        "-o", "--output",
        metavar = "FILE",
        action = "store",
        help = "File to which the dongers will be written. Writes to stdout, if omitted"
    )

    args = parser.parse_args()

    DEBUG = args.debug

    dongers = extract_dongers()
    donger_count = sum(len(c) for c in dongers.values())
    debug(f"Found {donger_count} dongers.")

    dongers_json = json.dumps(dongers, indent = 4)
    if args.output:
        with open(args.output, "w") as f:
            f.write(dongers_json)
    else:
        print(dongers_json)
