# donger (working title)
[![Build Status](https://travis-ci.org/albalitz/donger.svg)](https://travis-ci.org/albalitz/donger)

An Android app making dongers quickly and easily available on your phone.

The dongers displayed in this app are extracted from http://dongerlist.com.

## FAQ
1. **Q:** If the content is the same as dongerlist.com, why should I use your stupid app?  
  **A:** Its faster and you can mark dongers as favorites to access them even faster.
1. **Q**: I know an awesome, secret donger. Why isn't my favorite donger in your stupid app?  
  **A:** It can be soon, when I add adding custom dongers.

## Screenshots
| **Favorites** | **Overview** |
| :-----------: | :----------: |
| ![favorites](img/screenshot-favorites.png) | ![overview](img/screenshot-all.png) |
| **Categories** | **Category** |
| ![categories](img/screenshot-categories.png) | ![category](img/screenshot-category.png) |
