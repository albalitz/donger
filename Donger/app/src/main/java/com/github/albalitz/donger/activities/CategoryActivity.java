package com.github.albalitz.donger.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.GridView;

import com.github.albalitz.donger.R;
import com.github.albalitz.donger.db.Database;
import com.github.albalitz.donger.models.Category;
import com.github.albalitz.donger.models.Donger;
import com.github.albalitz.donger.utils.DongerListAdapter;

import java.util.ArrayList;

/**
 * Created by albalitz on 11/1/17.
 */

public class CategoryActivity extends AppCompatActivity implements Updatable {

    DongerListAdapter adapter;
    int categoryId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Intent intent = getIntent();
        if (!intent.hasExtra("category_id")) {
            Log.w(this.toString(), "Can't display category. Didn't get an ID with the intent.");
            finish();
        }
        categoryId = intent.getIntExtra("category_id", 0);

        Database database = new Database(this);

        Category category = database.getCategory(categoryId);
        ArrayList<Donger> dongers = database.getDongers(categoryId);

        getSupportActionBar().setTitle(category.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        GridView dongerList = (GridView) findViewById(R.id.dongerList);
        adapter = new DongerListAdapter(this, dongers, this);
        dongerList.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    public void update() {
        Database database = new Database(this);
        adapter.clear();
        adapter.addAll(database.getDongers(categoryId));
        adapter.notifyDataSetChanged();
    }
}
