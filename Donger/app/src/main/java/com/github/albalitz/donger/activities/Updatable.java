package com.github.albalitz.donger.activities;

/**
 * Created by albalitz on 10/31/17.
 */

public interface Updatable {
    void update();
}
