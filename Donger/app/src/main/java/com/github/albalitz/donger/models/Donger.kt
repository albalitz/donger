package com.github.albalitz.donger.models

/**
 * Created by albalitz on 10/30/17.
 */

class Donger(val representation: String, val isFavorite: Boolean = false, val id: Int = -1)