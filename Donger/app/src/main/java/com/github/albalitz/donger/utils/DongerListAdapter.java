package com.github.albalitz.donger.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.albalitz.donger.R;
import com.github.albalitz.donger.activities.Updatable;
import com.github.albalitz.donger.db.Database;
import com.github.albalitz.donger.models.Donger;

import java.util.ArrayList;

/**
 * Created by albalitz on 10/31/17.
 */

public class DongerListAdapter extends ArrayAdapter<Donger> {

    private Updatable updatable;

    public DongerListAdapter(Context context, ArrayList<Donger> dongers, Updatable updatable) {
        super(context, 0, dongers);

        this.updatable = updatable;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Donger donger = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.donger, parent, false);
        }

        EditText dongerRepresentation = (EditText) convertView.findViewById(R.id.dongerRepresentation);
        dongerRepresentation.setText(donger.getRepresentation());

        ImageButton buttonFavorite = (ImageButton) convertView.findViewById(R.id.buttonFavorite);
        buttonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Database database = new Database(getContext());
                if (donger.isFavorite()) {
                    database.removeDongerFromFavorites(donger);
                    Toast.makeText(getContext(), "Removed favorite.", Toast.LENGTH_SHORT).show();
                } else {
                    database.addDongerToFavorites(donger);
                    Toast.makeText(getContext(), "Added favorite.", Toast.LENGTH_SHORT).show();
                }
                if (updatable != null) {
                    updatable.update();
                }
            }
        });
        if (donger.isFavorite()) {
            buttonFavorite.setImageResource(R.drawable.ic_favorite_true);
        } else {
            buttonFavorite.setImageResource(R.drawable.ic_favorite_false);
        }

        ImageButton buttonCopy = (ImageButton) convertView.findViewById(R.id.buttonCopy);
        buttonCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboardManager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("donger", donger.getRepresentation());
                clipboardManager.setPrimaryClip(clip);
                Toast.makeText(getContext(), "Copied donger to clipboard.", Toast.LENGTH_SHORT).show();
            }
        });


        return convertView;
    }
}