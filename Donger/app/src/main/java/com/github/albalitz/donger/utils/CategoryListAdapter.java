package com.github.albalitz.donger.utils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.github.albalitz.donger.R;
import com.github.albalitz.donger.activities.CategoryActivity;
import com.github.albalitz.donger.activities.Updatable;
import com.github.albalitz.donger.db.Database;
import com.github.albalitz.donger.models.Category;
import com.github.albalitz.donger.models.Donger;

import java.util.ArrayList;

/**
 * Created by albalitz on 11/1/17.
 */

public class CategoryListAdapter  extends ArrayAdapter<Category> {

    public CategoryListAdapter(Context context, ArrayList<Category> categories) {
        super(context, 0, categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Category category = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.category, parent, false);
        }

        TextView categoryName = (TextView) convertView.findViewById(R.id.categoryName);
        categoryName.setText(category.getName());

        Database database = new Database(getContext());
        Donger exampleDonger = database.getFirstDongerFromCategory(category.getId());
        TextView exampleDongerTextView = (TextView) convertView.findViewById(R.id.exampleDonger);
        exampleDongerTextView.setText(exampleDonger.getRepresentation());

        Button viewCategoryButton = (Button) convertView.findViewById(R.id.viewCategoryButton);
        viewCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CategoryActivity.class);
                intent.putExtra("category_id", category.getId());
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }
}
