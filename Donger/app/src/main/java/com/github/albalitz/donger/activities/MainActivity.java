package com.github.albalitz.donger.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.github.albalitz.donger.R;
import com.github.albalitz.donger.db.Database;
import com.github.albalitz.donger.db.DatabaseSetup;
import com.github.albalitz.donger.fragments.AboutDialogFragment;
import com.github.albalitz.donger.models.Category;
import com.github.albalitz.donger.models.Donger;
import com.github.albalitz.donger.utils.CategoryListAdapter;
import com.github.albalitz.donger.utils.DongerListAdapter;

import org.json.JSONException;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Updatable {

    private Database database;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent splashScreenIntent = new Intent(this, SplashscreenActivity.class);
        startActivity(splashScreenIntent);

        setContentView(R.layout.activity_main);

        this.database = new Database(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            AboutDialogFragment.display(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        private Updatable updatable;
        private Database database;

        public PlaceholderFragment() {
        }

        @SuppressLint("ValidFragment")  // because I don't know how to pass that with a Bundle
        public PlaceholderFragment(Updatable updatable) {
            this.updatable = updatable;
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber, Updatable updatable) {
            PlaceholderFragment fragment = new PlaceholderFragment(updatable);
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            database = new Database(getContext());

            // depending on the tab, this is either a list of dongers, or a list of categories
            final GridView list = (GridView) rootView.findViewById(R.id.list);
            int tabNumber = getArguments().getInt(ARG_SECTION_NUMBER);
            switch (tabNumber) {
                case 2:
                    ArrayList<Category> categories = database.getCategories();
                    list.setAdapter(new CategoryListAdapter(getContext(), categories));
                    break;
                default:
                    ArrayList<Donger> dongers = getDongers(tabNumber);
                    list.setAdapter(new DongerListAdapter(getContext(), dongers, updatable));
                    break;
            }

            return rootView;
        }

        private ArrayList<Donger> getDongers(int tab) {
            switch (tab) {
                case 1:
                    return database.getDongers(-1);
                default:
                    return database.getFavorites();
            }
        }
    }

    /**
     * A {@link FragmentStatePagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position, MainActivity.this);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Favorites";
                case 1:
                    return "All";
                case 2:
                    return "Categories";
            }
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }


    @Override
    public void update() {
        mSectionsPagerAdapter.notifyDataSetChanged();
    }
}
