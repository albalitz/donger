package com.github.albalitz.donger.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by albalitz on 10/30/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Donger.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(this.toString(), "Creating database...");
        Log.d(this.toString(), "Creating category entries...");
        sqLiteDatabase.execSQL(DatabaseContract.SQL_CREATE_CATEGORY_ENTRIES);
        Log.d(this.toString(), "Creating donger entries...");
        sqLiteDatabase.execSQL(DatabaseContract.SQL_CREATE_DONGER_ENTRIES);
        Log.d(this.toString(), "Creating dongercategory entries...");
        sqLiteDatabase.execSQL(DatabaseContract.SQL_CREATE_DONGER_CATEGORY_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    public void clearAndRecreate() {
        Log.d(this.toString(), "Clearing and recreating database...");
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        clearDatabase();
        onCreate(sqLiteDatabase);
    }

    public void clearDatabase() {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(DatabaseContract.SQL_DROP_TABLE_CATEGORY);
        sqLiteDatabase.execSQL(DatabaseContract.SQL_DROP_TABLE_DONGER);
        sqLiteDatabase.execSQL(DatabaseContract.SQL_DROP_TABLE_DONGERCATEGORY);
    }
}
