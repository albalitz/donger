package com.github.albalitz.donger.models

/**
 * Created by albalitz on 10/30/17.
 */

class Category(val name: String, val id: Int = -1)
