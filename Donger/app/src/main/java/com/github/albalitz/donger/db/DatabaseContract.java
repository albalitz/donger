package com.github.albalitz.donger.db;

import android.provider.BaseColumns;

/**
 * Created by albalitz on 10/30/17.
 */

public final class DatabaseContract {

    protected static final String SQL_CREATE_CATEGORY_ENTRIES = "CREATE TABLE "
        + CategoryEntry.TABLE_NAME + "(" + CategoryEntry._ID + " INTEGER PRIMARY KEY,"
        + CategoryEntry.COLUMN_NAME + " TEXT UNIQUE"
        + ");";
    protected static final String SQL_CREATE_DONGER_ENTRIES = "CREATE TABLE "
        + DongerEntry.TABLE_NAME + " (" + DongerEntry._ID + " INTEGER PRIMARY KEY,"
        + DongerEntry.COLUMN_REPRESENTATION + " TEXT UNIQUE, "
        + DongerEntry.COLUMN_FAVORITE + " INTEGER"  // according to https://www.sqlite.org/datatype3.html#boolean_datatype
        + ");";
    protected static final String SQL_CREATE_DONGER_CATEGORY_ENTRIES = "CREATE TABLE "
        + DongerCategoryEntry.TABLE_NAME + "(" + DongerCategoryEntry._ID + " INTEGER PRIMARY KEY, "
        + DongerCategoryEntry.DONGER_ID + " INTEGER, "
        + DongerCategoryEntry.CATEGORY_ID + " INTEGER, "
        + "FOREIGN KEY(" + DongerCategoryEntry.DONGER_ID + ") REFERENCES " + DongerEntry.TABLE_NAME + "(" + DongerEntry._ID + "),"
        + "FOREIGN KEY(" + DongerCategoryEntry.CATEGORY_ID + ") REFERENCES " + CategoryEntry.TABLE_NAME + "(" + CategoryEntry._ID + ")"
        + ");";

    protected static final String SQL_DROP_TABLE_CATEGORY = "DROP TABLE " + CategoryEntry.TABLE_NAME;
    protected static final String SQL_DROP_TABLE_DONGER = "DROP TABLE " + DongerEntry.TABLE_NAME;
    protected static final String SQL_DROP_TABLE_DONGERCATEGORY = "DROP TABLE " + DongerCategoryEntry.TABLE_NAME;


    private DatabaseContract() {}

    public static class CategoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "category";
        public static final String COLUMN_NAME = "name";
    }

    public static class DongerEntry implements BaseColumns {
        public static final String TABLE_NAME = "donger";
        public static final String COLUMN_REPRESENTATION = "representation";
        public static final String COLUMN_FAVORITE = "favorite";
    }

    public static class DongerCategoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "dongercategory";
        public static final String DONGER_ID = "dongerid";
        public static final String CATEGORY_ID = "categoryid";
    }
}
