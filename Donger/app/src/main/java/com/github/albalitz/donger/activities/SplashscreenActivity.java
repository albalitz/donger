package com.github.albalitz.donger.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.github.albalitz.donger.BuildConfig;
import com.github.albalitz.donger.R;
import com.github.albalitz.donger.db.DatabaseSetup;

import org.json.JSONException;

import static com.github.albalitz.donger.db.DatabaseSetup.PREF_KEY_LAST_APP_VERSION;

/**
 * Created by albalitz on 11/2/17.
 */

public class SplashscreenActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        TextView splashDonger = findViewById(R.id.splash_donger);
        splashDonger.setText(getString(R.string.splash_donger));

        TextView splashMessage = findViewById(R.id.splash_message);
        splashMessage.setText(getString(R.string.splash_message));

        if (newAppVersion(this)) {
            try {
                DatabaseSetup.setUp(this);
            } catch (JSONException e) {
                Log.e(this.toString(), e.toString());
                Toast.makeText(this, "Error while initiating database. Please report this.", Toast.LENGTH_LONG).show();
            }
        } else {

        }

        finishAfterWaiting();
    }

    private void finishAfterWaiting() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
    }

    private static boolean newAppVersion(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int lastVersionCode = preferences.getInt(PREF_KEY_LAST_APP_VERSION, 0);
        int currentVersionCode = BuildConfig.VERSION_CODE;

        return currentVersionCode > lastVersionCode;
    }
}
