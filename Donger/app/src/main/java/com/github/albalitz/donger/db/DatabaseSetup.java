package com.github.albalitz.donger.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.albalitz.donger.BuildConfig;
import com.github.albalitz.donger.models.Category;
import com.github.albalitz.donger.models.Donger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

/**
 * This populates the database with initial data, if it isn't populated already.
 */
public class DatabaseSetup {

    private static final String FILENAME = "dongers.json";
    public static final String PREF_KEY_LAST_APP_VERSION = "last-app-version";

    public static void setUp(Context context) throws JSONException {
        JSONObject json = readDongersFromJSON(context);
        Database database = new Database(context);

        Log.i("DatabaseSetup", "Populating donger database ...");
        Iterator iterator = json.keys();
        while (iterator.hasNext()) {
            String categoryName = (String) iterator.next();
            // TODO: use kotlin's default parameters
            long categoryId = database.saveCategory(new Category(categoryName, -1));
            Category category = database.getCategory((int) categoryId);

            JSONArray dongerRepresentations = json.getJSONArray(categoryName);
            for (int dongerIdx = 0; dongerIdx < dongerRepresentations.length(); dongerIdx++) {
                String dongerRepresentation = dongerRepresentations.getString(dongerIdx);
                // TODO: use kotlin's default parameters
                long dongerId = database.saveDonger(new Donger(dongerRepresentation, false, -1));
                Donger donger = database.getDonger((int) dongerId);

                database.addDongerToCategory(donger, category);
            }
        }

        // make sure this isn't run every time the app is opened
        updateAppVersion(context);
    }

    private static JSONObject readDongersFromJSON(Context context) throws JSONException {
        BufferedReader reader = null;
        StringBuilder fileContent = new StringBuilder();
        try {
            InputStream inputStream = context.getAssets().open(FILENAME);
            reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

            String line;
            while ((line = reader.readLine()) != null) {
                fileContent.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {}
            }
        }

        return new JSONObject(fileContent.toString());
    }

    private static void updateAppVersion(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int currentVersionCode = BuildConfig.VERSION_CODE;
        preferences.edit().putInt(PREF_KEY_LAST_APP_VERSION, currentVersionCode).apply();
    }
}
