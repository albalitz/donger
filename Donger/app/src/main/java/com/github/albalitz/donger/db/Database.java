package com.github.albalitz.donger.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.github.albalitz.donger.models.Category;
import com.github.albalitz.donger.models.Donger;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Created by albalitz on 10/30/17.
 */

public class Database {

    private DatabaseHelper databaseHelper;

    private static int FAVORITE_TRUE = 1;
    private static int FAVORIE_FALSE = 0;

    public Database(Context context) {
        Log.d(this.toString(), "Creating Database instance.");
        this.databaseHelper = new DatabaseHelper(context);
    }

    public long saveDonger(Donger donger) {
        try {
            Donger existing = getDonger(donger.getRepresentation());
            return existing.getId();
        } catch (NoSuchElementException ignored) {}

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.DongerEntry.COLUMN_REPRESENTATION, donger.getRepresentation());
        values.put(DatabaseContract.DongerEntry.COLUMN_FAVORITE, donger.isFavorite() ? FAVORITE_TRUE : FAVORIE_FALSE);

        Log.d(this.toString(), "Saving donger to database: " + donger.getRepresentation() + " (favorite: " + donger.isFavorite() + ")");
        return db.insert(DatabaseContract.DongerEntry.TABLE_NAME, null, values);
    }

    /**
     * Get dongers from the given category from the database.
     * category id of -1 means all dongers.
     *
     * @param categoryId ID of the category to get dongers from, or -1 for all.
     * @return ArrayList of dongers matching the category.
     */
    public ArrayList<Donger> getDongers(int categoryId) {  // todo: use kotlin's default parameters
        if (categoryId != -1) {
            return getDongersByCategory(categoryId);
        }

        Log.d(this.toString(), "Getting all dongers from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.DongerEntry._ID,
            DatabaseContract.DongerEntry.COLUMN_REPRESENTATION,
            DatabaseContract.DongerEntry.COLUMN_FAVORITE
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.DongerEntry._ID + " " + sortDirection;

        Cursor cursor = db.query(
            DatabaseContract.DongerEntry.TABLE_NAME,
            projection,
            null,
            null,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        ArrayList<Donger> dongers = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry._ID));
            String representation = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry.COLUMN_REPRESENTATION));
            int favoriteValue = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry.COLUMN_FAVORITE));
            boolean favorite = favoriteValue == FAVORITE_TRUE;

            Donger donger = new Donger(representation, favorite, id);
            dongers.add(donger);
        }
        cursor.close();

        Log.d(this.toString(), "Found " + dongers.size() + " dongers.");
        return dongers;
    }

    /**
     * Get dongers from the given category from the database.
     * category id of -1 means all dongers.
     *
     * @param categoryId ID of the category to get dongers from.
     * @return ArrayList of dongers matching the category.
     */
    ArrayList<Donger> getDongersByCategory(int categoryId) {
        Log.d(this.toString(), "Getting dongers from category " + categoryId + " from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.DongerCategoryEntry._ID,
            DatabaseContract.DongerCategoryEntry.DONGER_ID,
            DatabaseContract.DongerCategoryEntry.CATEGORY_ID
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.DongerCategoryEntry._ID + " " + sortDirection;

        String selection = DatabaseContract.DongerCategoryEntry.CATEGORY_ID + " = ?";
        String[] selectionArgs = { Integer.toString(categoryId) };

        Cursor cursor = db.query(
            DatabaseContract.DongerCategoryEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        ArrayList<Donger> dongers = new ArrayList<>();
        while (cursor.moveToNext()) {
            int dongerId = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerCategoryEntry.DONGER_ID));

            Donger donger = getDonger(dongerId);
            dongers.add(donger);
        }
        cursor.close();

        Log.d(this.toString(), "Found " + dongers.size() + " favorites.");
        return dongers;
    }

    public Donger getDonger(int dongerId) {
        Log.d(this.toString(), "Getting donger " + dongerId + " from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.DongerEntry._ID,
            DatabaseContract.DongerEntry.COLUMN_REPRESENTATION,
            DatabaseContract.DongerEntry.COLUMN_FAVORITE
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.DongerEntry._ID + " " + sortDirection;

        // filter dongerId
        String selection = DatabaseContract.DongerEntry._ID + " = ?";
        String[] selectionArgs = { Integer.toString(dongerId) };

        Cursor cursor = db.query(
            DatabaseContract.DongerEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        ArrayList<Donger> dongers = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry._ID));
            String representation = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry.COLUMN_REPRESENTATION));
            int favoriteValue = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry.COLUMN_FAVORITE));
            boolean favorite = favoriteValue == FAVORITE_TRUE;

            Donger donger = new Donger(representation, favorite, id);
            dongers.add(donger);
        }
        cursor.close();

        if (dongers.isEmpty()) {
            Log.e(this.toString(), "Can't find donger with id " + dongerId);
            throw new NoSuchElementException("Can't find donger with id " + dongerId);
        }

        Donger donger = dongers.get(0);
        Log.d(this.toString(), "Found donger: " + donger.getRepresentation());
        return donger;
    }

    public Donger getDonger(String dongerRepresentation) {
        Log.d(this.toString(), "Getting donger " + dongerRepresentation + " from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.DongerEntry._ID,
            DatabaseContract.DongerEntry.COLUMN_REPRESENTATION,
            DatabaseContract.DongerEntry.COLUMN_FAVORITE
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.DongerEntry._ID + " " + sortDirection;

        // filter dongerId
        String selection = DatabaseContract.DongerEntry.COLUMN_REPRESENTATION + " = ?";
        String[] selectionArgs = { dongerRepresentation };

        Cursor cursor = db.query(
            DatabaseContract.DongerEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        ArrayList<Donger> dongers = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry._ID));
            String representation = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry.COLUMN_REPRESENTATION));
            int favoriteValue = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry.COLUMN_FAVORITE));
            boolean favorite = favoriteValue == FAVORITE_TRUE;

            Donger donger = new Donger(representation, favorite, id);
            dongers.add(donger);
        }
        cursor.close();

        if (dongers.isEmpty()) {
            Log.e(this.toString(), "Can't find donger with name " + dongerRepresentation);
            throw new NoSuchElementException("Can't find donger with name " + dongerRepresentation);
        }

        Donger donger = dongers.get(0);
        Log.d(this.toString(), "Found donger: " + donger.getRepresentation());
        return donger;
    }

    public Donger getFirstDongerFromCategory(int categoryId) {
        Log.d(this.toString(), "Getting dongers from category " + categoryId + " from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.DongerCategoryEntry._ID,
            DatabaseContract.DongerCategoryEntry.DONGER_ID,
            DatabaseContract.DongerCategoryEntry.CATEGORY_ID
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.DongerCategoryEntry._ID + " " + sortDirection;

        String selection = DatabaseContract.DongerCategoryEntry.CATEGORY_ID + " = ?";
        String[] selectionArgs = { Integer.toString(categoryId) };

        Cursor cursor = db.query(
            DatabaseContract.DongerCategoryEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        Donger donger = null;
        if (cursor.moveToNext()) {
            int dongerId = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerCategoryEntry.DONGER_ID));

             donger = getDonger(dongerId);
        }
        cursor.close();

        return donger;
    }

    /**
     * Get dongers marked as favorites.
     *
     * @return ArrayList of the best dongers
     */
    public ArrayList<Donger> getFavorites() {
        Log.d(this.toString(), "Getting favorite dongers from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.DongerEntry._ID,
            DatabaseContract.DongerEntry.COLUMN_REPRESENTATION,
            DatabaseContract.DongerEntry.COLUMN_FAVORITE
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.DongerEntry._ID + " " + sortDirection;

        // filter favorite
        String selection = DatabaseContract.DongerEntry.COLUMN_FAVORITE + " = ?";
        String[] selectionArgs = { Integer.toString(FAVORITE_TRUE) };

        Cursor cursor = db.query(
            DatabaseContract.DongerEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        ArrayList<Donger> dongers = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry._ID));
            String representation = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry.COLUMN_REPRESENTATION));
            int favoriteValue = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.DongerEntry.COLUMN_FAVORITE));
            boolean favorite = favoriteValue != 0;

            Donger donger = new Donger(representation, favorite, id);
            dongers.add(donger);
        }
        cursor.close();

        Log.d(this.toString(), "Found " + dongers.size() + " favorites.");
        return dongers;
    }

    public void addDongerToFavorites(Donger donger) {
        Log.d(this.toString(), "Adding donger to favorites: " + donger.getRepresentation() + " (id " + donger.getId() + ")");
        setDongerFavorite(donger, true);
    }

    public void removeDongerFromFavorites(Donger donger) {
        Log.d(this.toString(), "Removing donger from favorites: " + donger.getRepresentation() + " (id " + donger.getId() + ")");
        setDongerFavorite(donger, false);
    }

    private void setDongerFavorite(Donger donger, boolean favorite) {
        int newFavoriteValue = favorite ? FAVORITE_TRUE : FAVORIE_FALSE;

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues newValues = new ContentValues();
        newValues.put(DatabaseContract.DongerEntry.COLUMN_FAVORITE, String.valueOf(newFavoriteValue));

        String selection = DatabaseContract.DongerEntry._ID + " = ?";
        String[] selectionArgs = { Integer.toString(donger.getId()) };

        int count = db.update(DatabaseContract.DongerEntry.TABLE_NAME, newValues, selection, selectionArgs);
        Log.d(this.toString(), "Updated " + count + " rows.");
    }

    public long saveCategory(Category category) {
        try {
            Category existing = getCategory(category.getName());
            return existing.getId();
        } catch (NoSuchElementException ignored) {}

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.CategoryEntry.COLUMN_NAME, category.getName());

        Log.d(this.toString(), "Saving category to database: " + category.getName() + ")");
        return db.insert(DatabaseContract.CategoryEntry.TABLE_NAME, null, values);
    }

    /**
     * Get all categories from the database.
     *
     * @return ArrayList of all the categories
     */
    public ArrayList<Category> getCategories() {
        Log.d(this.toString(), "Getting all categories from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.CategoryEntry._ID,
            DatabaseContract.CategoryEntry.COLUMN_NAME
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.DongerCategoryEntry._ID + " " + sortDirection;

        // not filtering anything here
        String selection = null;
        String[] selectionArgs = null;

        Cursor cursor = db.query(
            DatabaseContract.CategoryEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        ArrayList<Category> categories = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.CategoryEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CategoryEntry.COLUMN_NAME));

            Category category = new Category(name, id);
            categories.add(category);
        }
        cursor.close();

        Log.d(this.toString(), "Found " + categories.size() + " categories.");
        return categories;
    }

    public Category getCategory(int categoryId) {
        Log.d(this.toString(), "Getting category " + categoryId + " from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.CategoryEntry._ID,
            DatabaseContract.CategoryEntry.COLUMN_NAME
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.CategoryEntry._ID + " " + sortDirection;

        // filter categoryId
        String selection = DatabaseContract.CategoryEntry._ID + " = ?";
        String[] selectionArgs = { Integer.toString(categoryId) };

        Cursor cursor = db.query(
            DatabaseContract.CategoryEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        ArrayList<Category> categories = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.CategoryEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CategoryEntry.COLUMN_NAME));

            Category category = new Category(name, id);
            categories.add(category);
        }
        cursor.close();

        if (categories.isEmpty()) {
            Log.e(this.toString(), "Can't find category with id " + categoryId);
            throw new NoSuchElementException("Can't find category with id " + categoryId);
        }

        Category category = categories.get(0);
        Log.d(this.toString(), "Found category: " + category.getName());
        return category;
    }

    public Category getCategory(String categoryName) {
        Log.d(this.toString(), "Getting category " + categoryName + " from database...");
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] projection = {
            DatabaseContract.CategoryEntry._ID,
            DatabaseContract.CategoryEntry.COLUMN_NAME
        };

        String sortDirection = "ASC";
        String sortOrder = DatabaseContract.CategoryEntry._ID + " " + sortDirection;

        // filter categoryId
        String selection = DatabaseContract.CategoryEntry.COLUMN_NAME + " = ?";
        String[] selectionArgs = { categoryName };

        Cursor cursor = db.query(
            DatabaseContract.CategoryEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,  // don't group
            null,  // don't filter
            sortOrder
        );

        ArrayList<Category> categories = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.CategoryEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CategoryEntry.COLUMN_NAME));

            Category category = new Category(name, id);
            categories.add(category);
        }
        cursor.close();

        if (categories.isEmpty()) {
            Log.e(this.toString(), "Can't find category with name " + categoryName);
            throw new NoSuchElementException("Can't find category with name " + categoryName);
        }

        Category category = categories.get(0);
        Log.d(this.toString(), "Found category: " + category.getName());
        return category;
    }

    public long addDongerToCategory(Donger donger, Category category) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.DongerCategoryEntry.DONGER_ID, donger.getId());
        values.put(DatabaseContract.DongerCategoryEntry.CATEGORY_ID, category.getId());

        Log.d(this.toString(), "Adding donger " + donger.getId() + " to category " + category.getId() + ")");
        return db.insert(DatabaseContract.DongerCategoryEntry.TABLE_NAME, null, values);
    }

    public void removeDongerFromCategory(Donger donger, Category category) {
        Log.d(this.toString(), "Removing donger " + donger.getId() + " from category " + category.getId());
        String selection = DatabaseContract.DongerCategoryEntry.DONGER_ID + " = ? "
            + "AND " + DatabaseContract.DongerCategoryEntry.CATEGORY_ID + " = ?";
        String[] selectionArgs = { Integer.toString(donger.getId()), Integer.toString(category.getId()) };

        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.delete(DatabaseContract.DongerCategoryEntry.TABLE_NAME, selection, selectionArgs);
    }
}
