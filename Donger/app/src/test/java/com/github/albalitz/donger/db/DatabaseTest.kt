package com.github.albalitz.donger.db

import android.os.Build.VERSION_CODES.LOLLIPOP
import com.github.albalitz.donger.BuildConfig
import com.github.albalitz.donger.models.Category
import com.github.albalitz.donger.models.Donger
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config


@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(LOLLIPOP), packageName = "com.github.albalitz.donger")
class DatabaseTest {
    lateinit var dbHelper: DatabaseHelper
    lateinit var db: Database


    @Before
    fun setup() {
        dbHelper = DatabaseHelper(RuntimeEnvironment.application)
        dbHelper.clearAndRecreate()
        db = Database(RuntimeEnvironment.application)
    }

    @Test
    @Throws(Exception::class)
    fun testDbInsertion() {
        val donger = Donger("(^.^)", true)
        db.saveDonger(donger)

        val savedDonger: Donger = db.getDonger(1)
        assertEquals(savedDonger.representation, donger.representation)
        assertTrue(savedDonger.isFavorite)

        assertEquals(1, db.getDongers(-1).size)

        val category = Category("Test Category")
        db.saveCategory(category)
        assertEquals(1, db.categories.size)
    }

    @Test
    @Throws(Exception::class)
    fun testFavorites() {
        val dongers = ArrayList<Donger>()
        dongers.add(Donger("(^.^)", true))
        dongers.add(Donger("-.-", false))
        for (donger in dongers) {
            db.saveDonger(donger)
        }

        assertEquals(2, db.getDongers(-1).size)
        assertEquals(1, db.favorites.size)

        db.removeDongerFromFavorites(db.getDonger(1))
        db.removeDongerFromFavorites(db.getDonger(2))
        assertTrue(db.favorites.isEmpty())

        db.addDongerToFavorites(db.getDonger(1))
        db.addDongerToFavorites(db.getDonger(2))
        assertEquals(2, db.favorites.size)
    }

    @Test
    @Throws(Exception::class)
    fun testCategories() {
        val donger = Donger("(^.^)", true)
        db.saveDonger(donger)

        val category1 = Category("Test Category")
        val category2 = Category("Another category")
        db.saveCategory(category1)
        db.saveCategory(category2)

        assertTrue(db.getDongersByCategory(1).isEmpty())
        assertTrue(db.getDongersByCategory(2).isEmpty())

        db.addDongerToCategory(db.getDonger(1), db.getCategory(1))
        assertEquals(1, db.getDongersByCategory(1).size)
        assertTrue(db.getDongersByCategory(2).isEmpty())

        db.addDongerToCategory(db.getDonger(1), db.getCategory(2))
        assertEquals(1, db.getDongersByCategory(1).size)
        assertEquals(1, db.getDongersByCategory(2).size)

        db.removeDongerFromCategory(db.getDonger(1), db.getCategory(1))
        db.removeDongerFromCategory(db.getDonger(1), db.getCategory(2))
        assertTrue(db.getDongersByCategory(1).isEmpty())
        assertTrue(db.getDongersByCategory(2).isEmpty())
    }

    @After
    fun tearDown() {
        dbHelper.clearDatabase()
    }
}
